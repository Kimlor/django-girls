#Django'dan forms'u ve projenin modellerinden Post import edildi.

from django import forms
from .models import Post

#PostForm adında bir sınıf oluşturup bu sınıfa Django'nun ModelForm'u atandı.
#Meta sınıf belirtip bu sınıfı oluşturacak model'e kendi Post modelimizi atadık.
#Modelden hangi alanları istediğimi belirttim.

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'text',)