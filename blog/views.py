from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Post
from .forms import PostForm

#post_list'in görüntüyü isteyebilmesini sağladık ve bu çağırılan görüntüye html dosyası bağlandı.
#Sadece yine sondaki parantezin anlamı tam olarak ne onu anlamadım.
#posts= diye başlayan line'da önce published date'i olup olmamasına göre filtreleyip olanların oublished date'e göre sıralanmasını sağladım.
#request'e genel görünümün yanı sıra post'ların da görünümünü de çağırmasını söyledim.
#post details'ta kullanılmak üzere 404 hatası import edildi
#post detail fonksiyonu oluşturulup bu fonksiyona istenen postu isteyip bunu post detail view üzerinden gösterilmesi ve sayfanın olmaması durumunda 404 hatası alınması sağlandı.
#forms üzerinden postfıorm sınıfını import edip bunu oluşturduğum post_new sınıfında form olarak tanımlayıp post_edit'in html dosyayını ve form'u renderlamasını sağladım.
def post_list(request):
    posts= Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    return render(request, 'blog/post_list.html', {'posts': posts})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})

def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.publihed_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form':form})

#if döngüsü ile önce POST methodu istenmesi durumunda post'un alan kontrolu yapıldı ve kontrolden geçmesi durumunda author ve published date çekerek kaydedilmesi sağlandı
#Lakin niye commit=false gerekli?
#en son kısma da post kaydedildekten sonra post'un görüntülenebilmesi için post_detail sayfasına yönlendirilmesi yapıldı.

def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})
#post_new ile farkı var ama bu farkların nasıl işlediğini tam olarak anlamadım sadece postun primary keyini istiyoruz ki postu editleyebilelim. Bi primary key gelmemesi durumunda da hata verdirtiyoruz sanırsam.
