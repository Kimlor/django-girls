#from ... import ... ile kullanılacak kütüphaneleri tanımladım.

from django.db import models
from django.conf import settings
from django.utils import timezone

#Post isminde bir sınıf oluşturup bunu bir model olarak tanımladım.
#Author ismiyle postu atacak kişiyi tanımlayıp buna bi ForeignKey atanmasını sağladım. Silinmesi durumunu da CASCADE ile belirttim.
#Devamında ise her bir özelliğin alan tanımlamaları yapıldı. (Sadece published date'i tam anlamadım.)
#İlk fonksiyonda yayınlama (publish) anında posta o anki zamanı atayıop kaydetmeyi sağladım.
#İkinci fonksiyon postun başlığını string olarak döndürmek için ama tam olarak neden ihtiyaç duyulduğunu anlamadım.

class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title