# '.' path yazmak yerine mi kullanıldı yoksa başka amacı var mı?

from django.urls import path
from . import views
#url'lere post_name ekleyip buna bi görünüm atanması yapıldı.
#Sadece neden '' kullanıldı tam emin değilim. (URL'e sitenin ana ismini çekmesi için?)
#url'lere her bir post için ayrı bir link oluşturması sağlandı. /post ile link post ile başlatıldı, Devamındaki /<int:pk> ile linkin devamına postun primary key'i integer olarak atandı./ ve devamı ile de post_details view'ı çağrıldı.
#yeni post olustulacak url'i belirleyip buna post_new view'ını atayıp post_new adını almasını sağladım.
urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('post/<int:pk>/', views.post_detail, name='post_detail'),
    path('post/new', views.post_new, name='post_new'),
    path('post/<int:pk>/edit/', views.post_edit, name='post_edit')
]
